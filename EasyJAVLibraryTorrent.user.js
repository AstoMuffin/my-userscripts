// ==UserScript==
// @name        Easy JAV Library Torrent
// @author      AstoMuffin
// @description 12/02/2023, 5:08:01 pm
// @version     1.0
// @updateURL   https://gitlab.com/AstoMuffin/my-userscripts/-/raw/main/EasyJAVLibraryTorrent.user.js
// @match       https://www.javlibrary.com/en/?v=*
// @run-at      document-idle
// @icon         https://www.google.com/s2/favicons?sz=64&domain=javlibrary.com
// @grant        none
// ==/UserScript==

(function() {
  'use strict';

  const dvdIdElement = document.querySelector('#video_id .text');
  if (!dvdIdElement) {
    const errorMessage = document.createElement('p');
    errorMessage.style.color = 'red';
    errorMessage.textContent = 'Failed to find DVD ID element.';
    const element = document.querySelector('#video_id td.text');
    element.appendChild(errorMessage);
    return;
  }

  const dvdId = dvdIdElement.textContent.trim();
  if (!dvdId) {
    const errorMessage = document.createElement('p');
    errorMessage.style.color = 'red';
    errorMessage.textContent = 'Failed to extract DVD ID.';
    const element = document.querySelector('#video_id td.text');
    element.appendChild(errorMessage);
    return;
  }

  const searchURL = `https://sukebei.nyaa.si/?f=0&c=0_0&q=${dvdId}&s=size&o=desc`;

  const element = document.querySelector('#video_id td.text');

  if (!element.querySelector('.copy-link')) {
    const copyLink = document.createElement('a');
    copyLink.className = 'copy-link';
    copyLink.textContent = '⧉';
    copyLink.addEventListener('click', () => {
      navigator.clipboard.writeText(dvdId).then(() => {
        copyLink.classList.add('success');
        setTimeout(() => {
          copyLink.classList.remove('success');
        }, 1000);
      }).catch(() => {
        copyLink.classList.add('error');
        setTimeout(() => {
          copyLink.classList.remove('error');
        }, 1000);
      });
    });
    element.appendChild(copyLink);
  }

  if (!element.querySelector('.download-link')) {
    const downloadLink = document.createElement('a');
    downloadLink.className = 'download-link';
    downloadLink.textContent = '🡇';
    downloadLink.href = searchURL;
    element.appendChild(downloadLink);
  }

  document.addEventListener('keydown', (event) => {
    if (event.key === 'q' || event.key === 'Q') {
      event.preventDefault();
      window.open(searchURL, '_self');
    }
  });
})();